package labmobile.thisboss;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

/**
 * Created by Firdaus Dwika on 2/26/2016.
 */
public class ListAdapter extends ArrayAdapter<String> {
    private final Context context;
    private final String[] status, nama;
    private final String[] updated;
    private final String[] id_status;

    public ListAdapter(Context context, String[] nama, String[] status, String[] updated, String[] id){
        super(context, R.layout.list_item, nama);
        this.context = context;
        this.nama = nama;
        this.status = status;
        this.updated = updated;
        this.id_status = id;
    }

    @Override
    public View getView(int pos, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater
                .inflate(R.layout.list_item, parent, false);

        TextView tvid = (TextView) rowView.findViewById(R.id.id_status);
        TextView tvnama = (TextView) rowView.findViewById(R.id.nama);
        TextView tvstatus = (TextView) rowView.findViewById(R.id.status);
        TextView tvupdated = (TextView) rowView.findViewById(R.id.updated);

        tvid.setVisibility(View.GONE);

        tvstatus.setText(status[pos]);
        tvnama.setText(nama[pos]);
        tvupdated.setText(updated[pos]);// new DownloadImageTask((ImageView) rowView.findViewById(R.id.foto)).execute(foto[pos]);
        tvid.setText(id_status[pos]);
        return rowView;
    }
}
