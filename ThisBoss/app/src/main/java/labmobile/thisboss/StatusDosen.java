package labmobile.thisboss;

/**
 * Created by Firdaus Dwika on 2/3/2016.
 */
public class StatusDosen {
    public static String username,status,foto,nama,updated,id;

    public void setId(String id) {
        this.id = id;
    }

    public String getFoto() {
        return foto;
    }

    public String getId() {
        return id;
    }

    public String getStatus() {
        return status;
    }

    public String getUsername() {
        return username;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
