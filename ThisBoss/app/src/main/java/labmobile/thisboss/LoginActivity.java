package labmobile.thisboss;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class LoginActivity extends AppCompatActivity {
    Button login;
    EditText inputNip,inputPassword;
    ProgressDialog progressDialog;
    SessionManager sessionManager;
    SharedPreferences sharedPreferences;
    JSONParser jsonParser = new JSONParser();
    private static final String LOGIN_URL = "http://ubdbest.com/android_login_api/login_wmb.php";
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_MESSAGE = "message";
    public static final String SP = "sp";
    public static final String KEY = "username";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        login = (Button)findViewById(R.id.btnLogin);

        inputNip = (EditText)findViewById(R.id.name);
        inputPassword = (EditText)findViewById(R.id.password);

        sessionManager = new SessionManager(getApplicationContext());

        if(sessionManager.isLoggedIn()){
            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(intent);
            finish();
        }

        sharedPreferences = getSharedPreferences(SP, Context.MODE_PRIVATE);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username = inputNip.getText().toString().trim();
                String password = inputPassword.getText().toString().trim();

                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString(KEY,username);
                editor.commit();
                if(!username.isEmpty()&&!password.isEmpty()){
                    new AttemptLogin().execute();
                }else {
                    Toast.makeText(getApplicationContext(),"Username dan Password harap Diisi",Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        if (id == R.id.refresh) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    class AttemptLogin extends AsyncTask<String,String,String>{

        boolean failure = false;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(LoginActivity.this);
            progressDialog.setMessage("Please Wait......");
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            int success;
            String username = inputNip.getText().toString().trim();
            String password = inputPassword.getText().toString().trim();

            try{
                List<NameValuePair> para = new ArrayList<NameValuePair>();
                para.add(new BasicNameValuePair("username", username));
                para.add(new BasicNameValuePair("password", password));

                Log.d("request!", "starting");
                JSONObject jsonObject = jsonParser.makeHttpRequest(LOGIN_URL,
                        "POST",para);

                Log.d("Login Attempt", jsonObject.toString());

                success = jsonObject.getInt(TAG_SUCCESS);
                if(success == 1){
                    Log.d("Successfully Login!", jsonObject.toString());

                    sessionManager.setLogin(true);
                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    intent.putExtra("username",username);
                    finish();
                    startActivity(intent);

                    return jsonObject.getString(TAG_MESSAGE);
                }else{
                    return jsonObject.getString(TAG_MESSAGE);
                }

            }catch (JSONException e){
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            progressDialog.dismiss();
            if(s!=null){
                Toast.makeText(LoginActivity.this,s,Toast.LENGTH_LONG).show();
            }
        }
    }
}
