package labmobile.thisboss;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MainActivity extends AppCompatActivity {

    private Toolbar toolbar;
    SessionManager sessionManager;
    TextView nama,status;
    String username,state;
    SharedPreferences sharedPreferences;
    public static final String SP = "sp";
    public static final String KEY = "username";

    private String[] id;
    private String[] statusdosen;
    private String[] foto;
    private String[] name;
    private String[] st;
    private String[] updated;
    private String[] id_status;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        nama = (TextView)findViewById(R.id.name);
        status = (TextView)findViewById(R.id.status);

        sharedPreferences = getSharedPreferences(SP,MODE_PRIVATE);
        username = sharedPreferences.getString(KEY, null);
        nama.setText(username);

        toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        sessionManager = new SessionManager(getApplicationContext());

        ImageButton edit = (ImageButton)findViewById(R.id.edit);
        FloatingActionButton create = (FloatingActionButton)findViewById(R.id.fab);
        new GetData().execute("http://ubdbest.com/android_login_api/users.php?username="+username);
        new ShowData().execute("http://ubdbest.com/android_login_api/getUsers.php?username="+username);

        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), EditStatusActivity.class);
                startActivity(intent);
                finish();
            }
        });

        create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), MapsActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    private void logoutUser() {
        sessionManager.setLogin(false);

        // Launching the login activity
        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            logoutUser();
            return true;
        }

        if (id == R.id.refresh) {
            finish();
            startActivity(getIntent());
            return true;
        }

        if(id == R.id.change){
            Intent intent = new Intent(getBaseContext(),ChangePasswordActivity.class);
            startActivity(intent);
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    private class GetData extends AsyncTask<String, Void, String> {

        // Instansiasi class dialog
        ProgressDialog dialog = new ProgressDialog(MainActivity.this);
        String Content;
        String Error = null;
        // membuat object class JSONObject yang digunakan untuk menangkap data
        // dengan format json
        JSONObject jObject;
        // instansiasi class ArrayList
        ArrayList<NameValuePair> data = new ArrayList<NameValuePair>();

        @Override
        protected String doInBackground(String... params) {
            try {
                Content = CustomHttpClient.executeHttpPost(
                        "http://ubdbest.com/android_login_api/users.php?username="+username,
                        data);
            } catch (ClientProtocolException e) {
                Error = e.getMessage();
                cancel(true);
            } catch (IOException e) {
                Error = e.getMessage();
                cancel(true);
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return Content;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // menampilkan dialog pada saat proses pengambilan data dari
            // internet
            this.dialog.setMessage("Loading Data Status..");
            this.dialog.show();
        }

        @Override
        protected void onPostExecute(String result) {
            // menutup dialog saat pengambilan data selesai
            this.dialog.dismiss();
            if (Error != null) {
                Toast.makeText(getBaseContext(), "Error Connection Internet",
                        Toast.LENGTH_LONG).show();
            } else {
                try {
                    // instansiasi kelas JSONObject
                    jObject = new JSONObject(Content);
                    // mengubah json dalam bentuk array
                    JSONArray menuitemArray = jObject.getJSONArray("users");

                    // mendeskripsikan jumlah array yang bisa di tampung
                    id = new String[menuitemArray.length()];
                    statusdosen = new String[menuitemArray.length()];
                    name = new String[menuitemArray.length()];
                    // mengisi variable array dengan data yang di ambil dari
                    // internet yang telah dibuah menjadi Array
                    for (int i = 0; i < menuitemArray.length(); i++) {

                        id[i] = menuitemArray.getJSONObject(i)
                                .getString("id").toString();
                        statusdosen[i] = menuitemArray.getJSONObject(i)
                                .getString("status").toString();
                        name[i] = menuitemArray.getJSONObject(i)
                                .getString("nama").toString();
                        nama.setText(name[i]);
                        status.setText(statusdosen[i]);
                    }
                } catch (JSONException ex) {
                    Logger.getLogger(MainActivity.class.getName()).log(
                            Level.SEVERE, null, ex);
                }
            }
        }
    }

    private class ShowData extends AsyncTask<String, Void, String> {

        // Instansiasi class dialog
        ProgressDialog dialog = new ProgressDialog(MainActivity.this);
        String Content;
        String Error = null;
        // membuat object class JSONObject yang digunakan untuk menangkap data
        // dengan format json
        JSONObject jObject;
        // instansiasi class ArrayList
        ArrayList<NameValuePair> data = new ArrayList<NameValuePair>();

        @Override
        protected String doInBackground(String... params) {
            try {
                Content = CustomHttpClient.executeHttpPost(
                        "http://ubdbest.com/android_login_api/getUsers.php?username="+username,
                        data);
            } catch (ClientProtocolException e) {
                Error = e.getMessage();
                cancel(true);
            } catch (IOException e) {
                Error = e.getMessage();
                cancel(true);
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return Content;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // menampilkan dialog pada saat proses pengambilan data dari
            // internet
            this.dialog.setMessage("Loading Data..");
            this.dialog.show();
        }

        @Override
        protected void onPostExecute(String result) {
            // menutup dialog saat pengambilan data selesai
            this.dialog.dismiss();
            if (Error != null) {
                Toast.makeText(getBaseContext(), "Error Connection Internet",
                        Toast.LENGTH_LONG).show();
            } else {
                try {
                    // instansiasi kelas JSONObject
                    jObject = new JSONObject(Content);
                    // mengubah json dalam bentuk array
                    JSONArray menuitemArray = jObject.getJSONArray("users");

                    // mendeskripsikan jumlah array yang bisa di tampung
                    name = new String[menuitemArray.length()];
                    st = new String[menuitemArray.length()];
                    updated = new String[menuitemArray.length()];
                    id_status = new String[menuitemArray.length()];

                    // mengisi variable array dengan data yang di ambil dari
                    // internet yang telah dibuah menjadi Array
                    for (int i = 0; i < menuitemArray.length(); i++) {

                        name[i] = menuitemArray.getJSONObject(i)
                                .getString("nama").toString();
                        st[i] = menuitemArray.getJSONObject(i)
                                .getString("status").toString();
                        updated[i] = menuitemArray.getJSONObject(i)
                                .getString("created_at").toString();
                        id_status[i]= menuitemArray.getJSONObject(i)
                                .getString("id_status").toString();
                    }
                    // instansiasi class ListAdapter (Buka class ListAdapter)
//                    ListAdapter adapter = new ListAdapter(getBaseContext(),
//                            Nama, subNama, Gambar);
//                    setListAdapter(adapter);
                    ListAdapter adapter = new ListAdapter(getBaseContext(), name, st, updated,id_status);
                    ListView listview = (ListView) findViewById(R.id.listview);
                    listview.setAdapter(adapter);
//                    listview.setOnItemClickListener();

                    listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            StatusDosen.updated = updated[position];
                            StatusDosen.nama = name[position];
                            StatusDosen.status = st[position];
                            StatusDosen.id = id_status[position];
                            String ids = id_status[position];
                        }
                    });

                } catch (JSONException ex) {
                    Logger.getLogger(MainActivity.class.getName()).log(
                            Level.SEVERE, null, ex);
                }
            }
        }
    }
}
