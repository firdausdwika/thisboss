package labmobile.thisboss;

/**
 * Created by Firdaus Dwika on 1/26/2016.
 */

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class EditStatusActivity extends AppCompatActivity {

    private Toolbar toolbar;
    TextView hadir,keluarsebentar,dikampus,tidakhadir;
    EditText id,status;
    SessionManager sessionManager;
    SharedPreferences sharedPreferences;
    public static final String SP = "sp";
    public static final String KEY = "username";
    public static final String SUC = "success";
    private ProgressDialog progressDialog;
    private static String url_create_product = "http://ubdbest.com/android_login_api/edit.php";
    JSONParser jsonParser = new JSONParser();
    String isi;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_status);

        hadir = (TextView)findViewById(R.id.hadir);
        keluarsebentar = (TextView)findViewById(R.id.keluar);
        dikampus = (TextView)findViewById(R.id.kelas);
        tidakhadir = (TextView)findViewById(R.id.tidak);
        id = (EditText)findViewById(R.id.id);
        status = (EditText)findViewById(R.id.status);

        sharedPreferences = getSharedPreferences(SP, MODE_PRIVATE);
        String username = sharedPreferences.getString(KEY,null);
        id.setText(username);

        id.setVisibility(View.GONE);
        status.setVisibility(View.GONE);
        hadir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isi = "Hadir";
                status.setText(isi);
                String id_user = id.getText().toString().trim();
                String state = status.getText().toString().trim();

                if (!id_user.isEmpty() && !state.isEmpty()) {
                    new doUpdate().execute();
                } else {
                    Toast.makeText(getApplicationContext(),
                            "Please enter your details!", Toast.LENGTH_LONG)
                            .show();
                }
            }
        });

        keluarsebentar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isi = "Keluar Sebentar";
                status.setText(isi);
                String id_user = id.getText().toString().trim();
                String state = status.getText().toString().trim();

                if (!id_user.isEmpty() && !state.isEmpty()) {
                    new doUpdate().execute();
                } else {
                    Toast.makeText(getApplicationContext(),
                            "Please enter your details!", Toast.LENGTH_LONG)
                            .show();
                }
            }
        });

        dikampus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isi = "Di Kampus / Di Kelas";
                status.setText(isi);
                String id_user = id.getText().toString().trim();
                String state = status.getText().toString().trim();

                if (!id_user.isEmpty() && !state.isEmpty()) {
                    new doUpdate().execute();
                } else {
                    Toast.makeText(getApplicationContext(),
                            "Please enter your details!", Toast.LENGTH_LONG)
                            .show();
                }
            }
        });

        tidakhadir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isi = "Tidak Hadir";
                status.setText(isi);
                String id_user = id.getText().toString().trim();
                String state = status.getText().toString().trim();

                if (!id_user.isEmpty() && !state.isEmpty()) {
                    new doUpdate().execute();
                } else {
                    Toast.makeText(getApplicationContext(),
                            "Please enter your details!", Toast.LENGTH_LONG)
                            .show();
                }
            }
        });

        toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            logoutUser();
            return true;
        }

        if (id == R.id.refresh) {
            finish();
            startActivity(getIntent());
            return true;
        }

        if(id == R.id.change){
            Intent intent = new Intent(getBaseContext(),ChangePasswordActivity.class);
            startActivity(intent);
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    class doUpdate extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(EditStatusActivity.this);
            progressDialog.setMessage("Update Status.....");
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(true);
            progressDialog.show();
        }

        /**
         * Creating product
         * */
        protected String doInBackground(String... args) {
            String id_user = id.getText().toString();
            String state = status.getText().toString();

            // Building Parameters
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("username", id_user));
            params.add(new BasicNameValuePair("status", state));

            // getting JSON Object
            // Note that create product url accepts POST method
            JSONObject json = jsonParser.makeHttpRequest(url_create_product,
                    "POST", params);
            Log.d("response",json.toString());
            Intent i = new Intent(EditStatusActivity.this, MainActivity.class);
            startActivity(i);
            finish();
            return null;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            // dismiss the dialog once done
            progressDialog.dismiss();
        }

    }
    private void logoutUser() {
        sessionManager.setLogin(false);

        // Launching the login activity
        Intent intent = new Intent(EditStatusActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }
}

