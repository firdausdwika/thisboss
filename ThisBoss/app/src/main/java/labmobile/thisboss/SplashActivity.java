package labmobile.thisboss;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Window;

public class SplashActivity extends Activity {

    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_splash);

        Thread splashThread = new Thread(){
            public void run(){
                try{
                    int waited=0;
                    while (waited<2000){
                        sleep(100);
                        waited+=100;
                    }
                }catch (InterruptedException e){

                }finally{
                    Intent i = new Intent(getApplicationContext(),LoginActivity.class);
                    startActivity(i);
                    finish();
                }
            }
        };
        splashThread.start();
    }

}